/*
 * UART_driver.h
 *
 *  Created on: 05-05-2017
 *      Author: Adam
 */

#ifndef UART_DRIVER_H_
#define UART_DRIVER_H_

typedef enum UARTx_Tag
{
	UART_1 = 1,
	UART_2,
}UARTx_T;

typedef struct Description
{
	void ( *_UART_Init )(UARTx_T _UART);
	void ( *_UART_DeInit )(void);
};

typedef struct UART
{
	struct Description *_ptr;

};

void UART_Init(UARTx_T _UART);



#endif /* UART_DRIVER_H_ */
